from micropydd_restplus import VERSION


class RestplusConfig:
    MICROPYDD_RESTPLUS_VERSION: str = VERSION

    REST_API_VERSION: str = 'ApiVersion'
    REST_API_DESCRIPTION: str = 'ApiDescription'
    REST_API_NAME: str = 'ApiName'
    REST_ROOT = ''
