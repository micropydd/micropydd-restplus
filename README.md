[![pipeline status](https://gitlab.com/micropydd/micropydd-restplus/badges/master/pipeline.svg)](https://gitlab.com/micropydd/micropydd-restplus/commits/master) [![coverage report](https://gitlab.com/micropydd/micropydd-restplus/badges/master/coverage.svg)](https://gitlab.com/micropydd/micropydd-restplus/commits/master) [![PyPI version](https://badge.fury.io/py/MicroPyDD-restplus.svg)](https://badge.fury.io/py/MicroPyDD-restplus)

# MicroPyDD Flask-Restplus

This module is a simple wrapper that simplifies the setup of a restplus project. Furthermore adds some helper methods to the prohect:

```
GET /misc/config/
GET /misc/postman/
GET /misc/version/
GET /loggers/{id}/
GET /loggers/
POST /loggers/
``` 