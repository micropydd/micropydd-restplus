from micropydd_restplus.config import RestplusConfig


class MockRestplusConfig(RestplusConfig):
    S3_BUCKET = 'mock-bucket'


def test_config():
    # Given
    config = MockRestplusConfig()

    # When
    version = config.MICROPYDD_RESTPLUS_VERSION

    # Then
    assert version is not None
